
Para executar o projeto, rode:

```
$ npm install
$ node start
```

Abra três navegadores, digite mensagens na caixa de texto inferior e pressione ENTER. As mensagens serão entregues aos outros clientes via websockets através do servidor.

É necessário ter os seguintes pacotes instalados no sistema operacional:
- node
