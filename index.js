var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);

app.use(express.static('public'));

io.on('connection', function (cliente) {
  cliente.on('message', function (dados) {
    io.emit('broadcast', dados);
    console.log("Mensagem recebida:", dados);
  });
});

server.listen(3000);
