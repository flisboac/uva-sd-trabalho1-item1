var socket = io.connect("http://localhost:3000");

socket.on("connect", function() {
  console.log("Conectado ao servidor!");
});

socket.on("broadcast", function(data) {
  var elem = document.getElementById("log");
  elem.value = elem.value + "\n" + data;
  console.log("Mensagem recebida!", data);
});

function onNovaMensagem() {
  var elem = document.getElementById("msg");
  var texto = elem.value;
  elem.value = "";
  socket.emit("message", texto);
}
